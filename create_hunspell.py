#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas
import icu
import subprocess

def write_hunspell(words, file_name):
    f = open("hunspell/" + file_name, "w")
    f.write(str(len(words)) + "\n")
    words.to_csv(f, index=False, header=False)
    f.close()

words = pandas.read_csv("wikidata/wikidata_cs_lexemes.txt", ":", header=None)
wikidata_lemmas = words[0][words[2] == "L"]
# negation of verbs
verbs = words[0][words[1] == "v"][~words[0].isin(["jest", "je", "se"])]
words = words[0].append("ne" + verbs)
print("Wikidata\t" + str(len(words)))

files = ["k12.txt",  "k5-imp.txt",  "k5-min.txt",  "k5-prit.txt",  "k5-trp.txt"]
columns_for_blacklist = {"k12.txt": 0, "k5-imp.txt": 1}

for file in files:
    words_from_file = pandas.read_csv("data/generated/" + file, ":", header=None)
    count_from_file = len(words_from_file)
    if file == "k12.txt" or file == "k5-imp.txt":
        words_generated_blacklist = pandas.read_csv("data/blacklist_generated_" + file, header=None)[0]
        # apply blacklist and do not include words whose lemmas are available at Wikidata (currently only for a subset given by the first letter)
        words_from_file = words_from_file[1][(~words_from_file[columns_for_blacklist[file]].isin(words_generated_blacklist)) &
            ((~words_from_file[0].isin(wikidata_lemmas)) | (file != "k12.txt" or words_from_file[0] >= "b"))]
    else:
        words_from_file = words_from_file[1]

    if "k5" in file:
        # negation of verbs
        words_from_file = words_from_file.append("ne" + words_from_file)

    words_from_file = words_from_file[~words_from_file.isin(words)]
    print(file + "\t" + str(len(words_from_file)) + "/" + str(count_from_file))
    words = words.append(words_from_file, ignore_index=True)

# use only beginning of the file with most frequent and relevant words
words_src = pandas.read_csv("data/src/k12.txt", ":", header=None, nrows=7400)[0]
words_src_blacklist = pandas.read_csv("data/blacklist_src_k12.txt", header=None)[0]
words_src = words_src[~words_src.isin(words_src_blacklist)]
words_src = words_src[~words_src.isin(words)]
print("src/k12.txt\t" + str(len(words_src)) + "/7400")
words = words.append(words_src, ignore_index=True)

sort_key = icu.Collator.createInstance(icu.Locale("cs_CZ.UTF-8")).getSortKey
words = words.to_list()
words.sort(key=sort_key)
words = pandas.Series(words)

words = words.drop_duplicates()
write_hunspell(words, "cs_CZ_unmunched.dic")

munched = subprocess.check_output(["munch", "hunspell/cs_CZ_unmunched.dic", "hunspell/cs_CZ.aff"])
munched = munched.decode("utf-8").split("\n")
munched.sort(key=sort_key)
munched = munched[2:len(munched)] # remove number of lines and empty lines
munched = pandas.Series(munched)
write_hunspell(munched, "cs_CZ.dic")
