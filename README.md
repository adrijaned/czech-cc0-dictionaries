Czech CC0 dictionaries
======================

This repository contains Czech spell check dictionary (Hunspell), tools
to create it from the [Czech morphological dictionary](https://github.com/plin/slovnik)
and [Czech Wikidata lexemes](https://www.wikidata.org/wiki/Wikidata:Lexicographical_data)
and an extension to use it in the [LibreOffice](https://www.libreoffice.org/) suite.

The dictionary is considered as experimental (many words missing),
with a lot of space for improvements.

For online check of words, more details and instructions how to contribute,
see the webpage [ceskeslovniky.cz](http://ceskeslovniky.cz).

Installation of the LibreOffice extension
-----------------------------------------
1. Pack the files from the `libreoffice` directory to a single .zip file
or download the .oxt file from the [LibreOffice extension page](https://extensions.libreoffice.org/extensions/czech-cc0-dictionaries-ceske-cc0-slovniky).

2. In LibreOffice, choose `Tools - Extension Manager`, click the `Add` button
and select the .zip or .oxt file.

Adding new words
----------------
As the dictionary uses Wikidata lexemes, new words and their forms can be
easily added by adding them there:
1. Make sure that the word to be added is not already contained in Wikidata,
e.g. by searching at the [Ordia tools page](https://tools.wmflabs.org/ordia/)
(left top corner).

2. Create a lexeme at the [New Lexeme page](https://www.wikidata.org/wiki/Special:NewLexeme),
or use [friendlier templates](https://tools.wmflabs.org/lexeme-forms/) for some Czech parts of speech.
If a new form for an already existing lemma is needed, edit the existing lexeme page.

Creating Hunspell dictionary
----------------------------
1. Run `wikidata/get_cs_lexemes.py` (requires Python packages `SPARQLWrapper`, `pywikibot`,
`json` and `pandas`). This downloads the latest Czech Wikidata lexemes to the file `wikidata/wikidata_cs_lexemes.txt`.

1. Download [generated data of the Czech morphological dictionary](https://github.com/plin/slovnik/tree/master/generated)
to the `data/generated` directory and the file `k12.txt` from [source data](https://github.com/plin/slovnik/tree/master/src)
to the `data/src` directory.

2. Run `create_hunspell.py`. Hunspell dictionaries are created in the `hunspell` directory
as both unmunched and munched version (requires Hunspell with the `munch` tool).

License
-------
Licensed under the [Creative Commons CC0 License](https://creativecommons.org/publicdomain/zero/1.0/).