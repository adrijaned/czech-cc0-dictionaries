#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from SPARQLWrapper import SPARQLWrapper, JSON
import pywikibot
import json
import pandas

sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
sparql.setQuery("""SELECT ?l ?lemma  WHERE {
   ?l a ontolex:LexicalEntry ; dct:language wd:Q9056 ;
        wikibase:lemma ?lemma .
}""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

lexeme_ids = []
for result in results["results"]["bindings"]:
    lexeme_ids.append(result["l"]["value"].split("/")[-1])

site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()

words = []
words_cats = []
words_types = []
categories = {"Q1084": "n", "Q34698": "adj", "Q24905": "v"}
for lex in range(len(lexeme_ids)):
    if (lex % 10 == 0):
        print(str(lex) + "/" + str(len(lexeme_ids)))
    lexeme_page = pywikibot.Page(repo, lexeme_ids[lex], ns=146) # 146 means "Lexeme" namespace
    lexeme = json.loads(lexeme_page.get())

    is_colloquialism = len(lexeme["senses"]) > 0
    for sense in lexeme["senses"]:
        is_colloquialism_sense = False
        if "P6191" in sense["claims"]:
            for prop in sense["claims"]["P6191"]:
                # language style colloquial language, Common Czech
                if prop["mainsnak"]["datavalue"]["value"]["id"] in ["Q901711", "Q677531"]:
                    is_colloquialism_sense = True
                    break
        is_colloquialism = is_colloquialism and is_colloquialism_sense
        if not is_colloquialism:
            break

    if not is_colloquialism:
        category = lexeme["lexicalCategory"]
        if category in categories.keys():
            category = categories[category]
        else:
            category = ""
        new_words = []
        for language in lexeme["lemmas"]:
            if "cs" in language:
                new_words = new_words + lexeme["lemmas"][language]["value"].split(" ")
        for form in lexeme["forms"]:
            for language in form["representations"]:
                if "cs" in language:
                    new_words = new_words + form["representations"][language]["value"].split(" ")

        words = words + new_words
        words_cats = words_cats + [category] * len(new_words)
        words_types = words_types + ["L"] + ["F"] * (len(new_words) - 1)

words = pandas.DataFrame({"word": words, "cat": words_cats, "type": words_types})

f = open("wikidata_cs_lexemes.txt", "w")
words.to_csv(f, ":", index=False, header=False)
f.close()
