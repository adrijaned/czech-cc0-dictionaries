# Experimental Czech spell check dictionary for Mozilla applications

This is an experimental dictionary extension for Firefox. For common use please install the [dictionary from addons.mozilla.org](https://addons.mozilla.org/firefox/addon/czech-spell-checking-dictionar/).

## How to build

Simply run the `make` command which will yield and `.xpi` file into the `target` sub-folder.

## How to install

The self-build extension is not signed and Firefox will reject to install it directly from file. However you can install it temporarily (until restart) to *debug it*.

1. Open add-ons manager (`Ctrl`+`Shift`+`A`).
1. Click the *gear* button in the top right corner.
1. Select *Debug Add-ons* (*Ladění doplňků*).
1. Click *Load Temporary Add-on* (*Načíst dočasný doplněk*) button.
1. In file picker select the `.xpi` file from the `target` sub-folder.

## How to report issues

- If you are not happy with the results, please check this repository root folder on how to improve the data.
- If you have some issues with installing the extension into Firefox, please contact [Mozilla.cz](https://www.mozilla.cz/o-nas/). Do not forget to include a link to this repository, or explicitly mention you have issues with this **experimental dictionary**.
